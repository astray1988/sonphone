$(function() {
    var timer=null;
    $.extend({
        listFence:function(){
              $("#fence-list-dialog").load("/Fences/list"
        		,{'targetSn': selectedTargetSn}
        		,function(responseText, textStatus, XMLHttpRequest){
				    $.hideHistoryDialog();
				    $(".dialog").hide();
				    $("#fence-list-dialog").show();
	            }
		      );
		  },
		  targetManageList:function(){
			  $("#target-manage-dialog").load(
				"target/manage.htm"
				,{targetSn: selectedTargetSn}
                ,function(responseText, textStatus, XMLHttpRequest){
				    $.hideHistoryDialog();
				    $(".dialog").hide();
				    $("#target-manage-dialog").show();
				}
		      );
		  }
    });
    
    /**
     * 显示历史轨迹
     */
    $("#mLSGJ").click(function(){
        $(".dialog").hide();
        $("#history-dialog").show();
    });
    
    var gprsDataArray = null;
    
    /**
     * 围栏管理
     */
    $("#menu-target").click(function (){
  	  $.targetManageList();
    });
    
    /**
     * 间隔设置
     */
    $("#mDSDW").click(function(){
        $("#interval-dialog").load(
            "config/loadInterval.htm"
            ,{targetSn: selectedTargetSn}
            ,function(responseText, textStatus, XMLHttpRequest){
                $.hideHistoryDialog();
                $(".dialog").hide();
                $("#interval-dialog").show();
            }
        );
    });
    
    /**
     * 围栏列表
     */
    $("#mWLGL").click(function(){
        $.listFence();
    });
    
    /**
     * 设备设置
     */
    $("#mSJSZ").click(function(){
        $("#target-config-dialog").load(
            "Configs/show",{"targetSn": selectedTargetSn}
            ,function(responseText, textStatus, XMLHttpRequest){
                $.hideHistoryDialog();
                $(".dialog").hide();
                $("#target-config-dialog").show();
            }
        );
    });
    /**
     * 及时定位按钮失效
     */
    var disableOnTime = function(){
        $("#locateOnTime").unbind("click");
        $("#waitOnTime").show();
    };
   /**
    * 及时按钮生效
    */
    var enableOnTime = function(){
         $('#locateOnTime').bind("click", onTime);
        $("#waitOnTime").hide();
    };
    var onTime = function(){
    	$( "#dialog:ui-dialog" ).dialog( "destroy" );
		$( "#locateOnTime-dialog-confirm" ).dialog({
			resizable: false,
			height:160,
			modal: true,
			buttons: {
				"是": function() {
					$( this ).dialog( "close" );
		            $.hideHistoryDialog();
		            disableOnTime();
		            $.ajax({
		                url: 'target/status/locateOnTime.htm',
		                data: "targetSn="+selectedTargetSn,
		                success: function(result) {
		                	if(result=="onelimit"){
		                		alert("已到达当日发送短信上限(25条)");
		                        $("#waitOnTime").hide();
		                	}else if(result=="monthlimit"){
		                		alert("已到达当月发送短信上限(300条)");
		                        $("#waitOnTime").hide();
		                	}else{
		                		var target = $.parseJSON(result);
			                    if(target!=null) {
			                        enableOnTime();
			                        updateTargetMarker(target);
			                        
			                    }else{
			                        var i=0;
			                        timer = setInterval(function(){
			                            i++;
			                            callbackLocateOnTime(i);
			                        },30000);
			                    } 
		                	}
		                },
		                error: function (XMLHttpRequest, textStatus, errorThrown) { 
		                    $.pnotify({
		                        pnotify_title: '错误',
		                        pnotify_text: '即时定位时服务器发生错误！',
		                        pnotify_type: 'error'
		                    });
		                    enableOnTime();
		                }
		            });
				},
				"否": function() {
					$( this ).dialog( "close" );
				}
			}
		});
    };
    /**
     * 及时定位
     */
    $('#mJSDW').bind("click", onTime);
    
    /**
     *获取及时定位结果 
     */
    var callbackLocateOnTime =function(times){
        $.ajax({
            url: 'target/status/callbackLocateOnTime.htm',
            type:"post",
            data:{
                targetSn:selectedTargetSn,
                lastReportTime:$.findBySelectedTargetSn().reportTime
            },
            success: function(result) {
                var target = $.parseJSON(result);
                if(target!=null && times<=5){
                    $.updateBySelectedTargetSn(target);
                    if(target.statusCode=='0'){
                    	updateTargetMarker(target);
                        
                        $.pnotify({
                            pnotify_title: '提示',
                            pnotify_text: '定位成功',
                            pnotify_type: 'info'
                        });
                    }else{
                        $.pnotify({
                            pnotify_title: '错误',
                            pnotify_text: '发生'+target.statusCode+'错误！',
                            pnotify_type: 'error'
                        });
                    }
                    enableOnTime();
                    clearInterval(timer);
                }else if(times>6) {
                    $.pnotify({
                        pnotify_title: '错误',
                        pnotify_text: '定位超时！',
                        pnotify_type: 'error'
                    });
                    enableOnTime();
                    clearInterval(timer);
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) { 
                $.pnotify({
                    pnotify_title: '错误',
                    pnotify_text: '获取定位结果时服务器发生错误！',
                    pnotify_type: 'error'
                });
                clearInterval(timer);
                enableOnTime();
            }
        });
    };
    
    $("#clearMarker").click(function(){
        removeAllMarker();
        removeAllPolylines();
    });
   
    $("#operationResult").click(function(){
    	var operationResultClassName = $("div#operationResult span").attr('class');
    	if(operationResultClassName=="log_on") {
    		$("div#operationResult span").addClass("log_off") .removeClass("log_on");
    	}else{
        	$("div#operationResult span").addClass("log_on") .removeClass("log_off");
    	}
        if(($("#operation-div").is(":hidden"))){
            $.ajax({
                url: "config/listOperationResult.htm",
                type:"post",
                data: "targetSn="+selectedTargetSn,
                success: function(data) {  
                    gprsDataArray  = $.parseJSON(data);
                    if(gprsDataArray.length>0) {
                        var gprsDataList = "<ul>";
                        $.each(gprsDataArray, function(i, gprsData){
                            gprsDataList +="<li><em>"+ new Date(gprsData.gprsDate)
                               .format("MM-dd hh:mm")+"</em><span>"+gprsData.purpose;
                            if(gprsData.dataStatus=='F0') {
                                gprsDataList +="成功";
                            } else{
                                gprsDataList +="中";
                            }
                            gprsDataList +="</span></li>";
                        });
                        gprsDataList +="</ul><p></p>";
                        $("#operation-div").html(gprsDataList);
                    }else {
                        $.pnotify({
                            pnotify_title: '提示',
                            pnotify_text: '没有操作记录！',
                            pnotify_type: 'info'
                        });
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) { 
                    $.pnotify({
                        pnotify_title: '错误',
                        pnotify_text: '服务器发生错误！',
                        pnotify_type: 'error'
                    });
                }
           });
           $("#operation-div").show();
        }else{
            $("#operation-div").hide();
        }
    });
});