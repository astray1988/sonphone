$(function() {

    var rawKeyDial = [];

    var rawQuiet = [];

    var rawSmsRemind = [$("#powerOn").attr("checked"),$("#lowerPower").attr("checked")];

    var rawClock = [];

    $('.input-check').checkbox();

    var hideTargetConfig = function(){
        $('.dial-key').hide();
        $('.phone-book').hide();
        $(".interval").hide();
        $('.target_config').hide();
    };
    $(".his_input1").timePicker();
    $("input:button,input:submit,a.button").button();

    $("#config-icon-close-circle").click(function(){
        $("#target-config-dialog").hide();
        $("#targetConfig").toggleClass("selected",false);
    });/*关闭按钮*/
    $(".config-close").click(function(){
        hideTargetConfig();
        $('.target_config').show();
    });
    $("#dial-key-dialog-icon-close-circle").click(function(){
        var modifyFlag = false;
        var currentKeyDial = [];

        for(var i=0;i<5;i++) {
            currentKeyDial[i] = [$("#telephoneNumber"+i).val(),$("#nickName"+i).val()];
            for(var j=0;j<2;j++) {
                if(rawKeyDial[i][j] != currentKeyDial[i][j]) {
                    modifyFlag = true;
                    break;
                }
            }
        }

        if(modifyFlag) {
            $( "#dialog:ui-dialog" ).dialog( "destroy" );
            $( "#dial-key-dialog-confirm" ).dialog({
                resizable: false,
                modal: true,
                buttons: {
                    "是": function() {
                        $( this ).dialog( "close" );
                        $("#keyForm").submit();
                        hideTargetConfig();
                        $('.target_config').show();
                    },
                    "否": function() {
                        $( this ).dialog( "close" );
                        hideTargetConfig();
                        $('.target_config').show();
                        document.getElementById('keyForm').reset();
                    }
                }
            });
        } else {
            $( this ).dialog( "close" );
            hideTargetConfig();
            $('.target_config').show();
        }
    });

    $("#quiet-dialog-icon-close-circle").click(function(){
        var modifyFlag = false;
        var currentQuiet = [];

        for(var i=0;i<3;i++) {
            currentQuiet[i] = [$("#period"+i).val(),$("#startTime"+i).val(),$("#endTime"+i).val()];
            for(var j=0;j<3;j++) {
                if(rawQuiet[i][j] != currentQuiet[i][j]) {
                    modifyFlag = true;
                    break;
                }
            }
        }

        if(modifyFlag) {
            $( "#dialog:ui-dialog" ).dialog( "destroy" );
            $( "#quiet-dialog-confirm" ).dialog({
                resizable: false,
                modal: true,
                buttons: {
                    "是": function() {
                        $( this ).dialog( "close" );
                        $("#quietForm").submit();
                        hideTargetConfig();
                        $('.target_config').show();
                    },
                    "否": function() {
                        $( this ).dialog( "close" );
                        hideTargetConfig();
                        $('.target_config').show();
                        document.getElementById('quietForm').reset();
                    }
                }
            });
        } else {
            $( this ).dialog( "close" );
            hideTargetConfig();
            $('.target_config').show();
        }
    });

    $("#sms-remind-dialog-icon-close-circle").click(function(){
        var modifyFlag = false;
        var currentSmsRemind = [$("#powerOn").attr("checked"),$("#lowerPower").attr("checked")];
        for(var i=0;i<2;i++) {
            if(rawSmsRemind[i] != currentSmsRemind[i]) {
                modifyFlag = true;
                break;
            }
        }

        if(modifyFlag) {
            $( "#dialog:ui-dialog" ).dialog( "destroy" );
            $( "#sms-remind-dialog-confirm" ).dialog({
                resizable: false,
                modal: true,
                buttons: {
                    "是": function() {
                        $( this ).dialog( "close" );
                        $("#smsRemindButton").click();
                        hideTargetConfig();
                        $('.target_config').show();
                    },
                    "否": function() {
                        $( this ).dialog( "close" );
                        hideTargetConfig();
                        $('.target_config').show();
                        document.getElementById('smsRemindForm').reset();
                    }
                }
            });
        } else {
            $( this ).dialog( "close" );
            hideTargetConfig();
            $('.target_config').show();
        }
    });

    $("#clock-dialog-icon-close-circle").click(function(){
        var modifyFlag = false;
        var currentClock = [];
        for(var i=0;i<3;i++) {
            currentClock[i] = [$("#clockPeriod"+i).val(),$("#clockTime"+i).val(),$("#clockEnable"+i).attr("checked")];
            for(var j=0;j<3;j++) {
                if(rawClock[i][j]!=currentClock[i][j]) {
                    modifyFlag = true;
                    break;
                }
            }
        }

        if(modifyFlag) {
            $( "#dialog:ui-dialog" ).dialog( "destroy" );
            $( "#clock-dialog-confirm" ).dialog({
                resizable: false,
                modal: true,
                buttons: {
                    "是": function() {
                        $( this ).dialog( "close" );
                        $("#clockForm").submit();
                        hideTargetConfig();
                        $('.target_config').show();
                    },
                    "否": function() {
                        $( this ).dialog( "close" );
                        hideTargetConfig();
                        $('.target_config').show();
                        document.getElementById('clockForm').reset();
                    }
                }
            });
        } else {
            $( this ).dialog( "close" );
            hideTargetConfig();
            $('.target_config').show();
        }
    });

    /* $(".config-bg").corner(); *//* div圆角 */
    $("#dial-key").click(function(){
        hideTargetConfig();
        $('#dial-key-dialog').show();
    });
    $("#quietId").click(function(){
        hideTargetConfig();
        $('#quiet-dialog').show();
    });
    $("#whiteId").click(function(){
        hideTargetConfig();
        $('#white-dialog').show();
    });
    $("#sms-remind").click(function(){
        hideTargetConfig();
        $('#sms-remind-dialog').show();
    });
    $("#clockId").click(function(){
        hideTargetConfig();
        $('#clock-dialog').show();
    });
    $("#sms-receive").click(function(){
        hideTargetConfig();
        $('#sms-receive-dialog').show();
    });
    $("#phone-book").click(function(){
        hideTargetConfig();
        $('#phone-book-dialog').show();
    });

    var terminalStatusTimer;
    var bgObj;
    var msgObj;
    $("#terminal-shutdown").click(function(){
    	$( "#dialog:ui-dialog" ).dialog( "destroy" );
		$( "#terminal-shutdown-dialog-confirm" ).dialog({
			resizable: false,
			height:150,
			modal: true,
			buttons: {
				"是": function() {
					$( this ).dialog( "close" );
					hideTargetConfig();
		            $.hideHistoryDialog();
		            $.ajax({
		                url: 'config/shutDownTerminal.htm',
		                data: "targetSn="+selectedTargetSn,
		                success: function(result) {
		                    if(result) {
		                        var i=0;
		                        terminalStatusTimer = setInterval(function(){
		                            i++;
		                            callbackTerminalStatus(i);
		                        },10000);
		                        var msgw,msgh,bordercolor;
		                        msgw=300;//提示窗口的宽度
		                        msgh=200;//提示窗口的高度
		                        titleheight=25 //提示窗口标题高度
		                        bordercolor="#FF3C00";//提示窗口的边框颜色
		                        titlecolor="#D2CECE";//提示窗口的标题颜色

		                        var sWidth,sHeight;
		                        //sWidth=document.body.offsetWidth; //得出当前屏幕的宽
		                        sWidth=document.body.clientWidth;//BODY对象宽度

			                    if (window.innerHeight && window.scrollMaxY) {
			                        sHeight = window.innerHeight + window.scrollMaxY;
			                    }else if (document.body.scrollHeight > document.body.offsetHeight) {
			                        sHeight = document.body.scrollHeight;
			                    }else {
			                        sHeight = document.body.offsetHeight;
			                    }//以上得到整个屏幕的高

		                        bgObj=document.createElement("div");//创建一个div对象
		                        bgObj.setAttribute('id','bgDiv');//可以用bgObj.id="bgDiv"的方法，是为div指定属性值
		                        bgObj.style.position="absolute";//把bgDiv这个div绝对定位
		                        bgObj.style.top="0";//顶部为0
		                        bgObj.style.background="#777";//背景颜色
		                        bgObj.style.filter="progid:DXImageTransform.Microsoft.Alpha(style=3,opacity=25,finishOpacity=75)";//ie浏览器透明度设置
		                        bgObj.style.opacity="0.6";//透明度(火狐浏览器中)
		                        bgObj.style.left="0";//左边为0
		                        bgObj.style.width=sWidth + "px";//宽(上面得到的屏幕宽度)
		                        bgObj.style.height=sHeight + "px";//高(上面得到的屏幕高度)
		                        bgObj.style.zIndex = "100";//层的z轴位置
		                        $(document.body).append(bgObj);
		                        msgObj=document.createElement("div")//创建一个div对象
		                        msgObj.setAttribute("id","msgDiv");//可以用bgObj.id="msgDiv"的方法，是为div指定属性值
		                        msgObj.setAttribute("align","center");//为div的align赋值
		                        msgObj.style.background="white";//背景颜色为白色
		                        //msgObj.style.border="1px solid " + bordercolor;//边框属性，颜色在上面已经赋值
		                        msgObj.style.position = "absolute";//绝对定位
		                        msgObj.style.left = "40%";//从左侧开始位置
		                        msgObj.style.top = "30%";//从上部开始位置
		                        // msgObj.style.font="12px/1.6em Verdana, Geneva, Arial, Helvetica, sans-serif";//字体属性
		                        //msgObj.style.marginLeft = "-225px";//左外边距
		                        //msgObj.style.marginTop = -75+document.documentElement.scrollTop+"px";//上外边距
		                        msgObj.style.width = msgw + "px";//提示框的宽(上面定义过)
		                        msgObj.style.height =msgh + "px";//提示框的高(上面定义过)
		                        msgObj.style.textAlign = "center";//文本位置属性，居中。
		                        msgObj.style.lineHeight ="45px";//行间距
		                        msgObj.style.zIndex = "101";//层的z轴位置
		                        $(document.body).append(msgObj);//在body中画出提示框层
		                        $("#msgDiv").html("<br><img src='resources/images/waiting.gif' width='80px' height='80px'><br><font size='5'>亲子机关机中，请稍后...</font>");
		                        //6s后关闭msgDiv
		                        p = function(){
		                        	var omgs = document.getElementById('bgDiv');
			                        var mgs = document.getElementById('msgDiv');
			                        mgs.style.display = "none";
			                        omgs.removeAttribute('style');
		                        }
		                        setTimeout(p,6000);
			                }
		                },
		                error: function (XMLHttpRequest, textStatus, errorThrown) {
		                    $.pnotify({
		                        pnotify_title: '错误',
		                        pnotify_text: '立即关机时服务器发生错误！',
		                        pnotify_type: 'error'
		                    });
		                }
		            });
				},
				"否": function() {
					$( this ).dialog( "close" );
				}
			}
		});
    });

    /**
     *获取及时定位结果
     */
    var callbackTerminalStatus =function(times){
    	$.ajax({
            url: 'config/getTerminalStatus.htm',
            type:"post",
            data:{
                targetSn:selectedTargetSn
            },
            success: function(result) {
                if(result){
                    alert("关机成功!");
                    clearInterval(terminalStatusTimer);
                    document.body.removeChild(bgObj);//移除遮罩层
                    document.body.removeChild(msgObj);//移除提示框
                    $("#terminalStatus"+selectedTargetSn).html("<span>状态：</span><b>关机</b>");
                }else if(times>30) {
                    alert("关机失败!");
                    clearInterval(terminalStatusTimer);
                    document.body.removeChild(bgObj);//移除遮罩层
                    document.body.removeChild(msgObj);//移除提示框
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                $.pnotify({
                    pnotify_title: '错误',
                    pnotify_text: '获取亲子机开机状态时服务器发生错误！',
                    pnotify_type: 'error'
                });
                clearInterval(terminalStatusTimer);
            }
        });
    };

    $("li.li-white").mouseleave(function() {
        $(this).css("background","white");
    });

    $("li.li-white").click(function() {
        $(this).css("background","#e3d9cf");
    });

    $("li.li-white").hover(function(){$(this).css("background","#e3d9cf");},function(){$(this).css("background","white");});

    $("#whiteSmsNo").blur(function() {
        $.each($("#whiteNo li"),function(i){
            var whiteLiText = $(this).text();
            var whiteIndex = whiteLiText.indexOf("(");
            var whiteNo = whiteLiText.substring(0,whiteIndex);
            if($("#whiteSmsNo").val() == whiteNo) {
                $(this).css("background","#e3d9cf");
                $("#errorDiv").html("<font color='red'>该号码已存在</font>").show();
            }
        });
    });
    $("#whiteForm").validate({
        errorClass: "warning",
        errorLabelContainer: $("#whiteForm .error"),
        rules : {
            whiteName : {
                required : true,
                maxlength : 4
            },
            whiteSmsNo : {
                required : true,
                digits : true,
                range:[01000000000,189999999999]
            }
        },
        messages : {
            whiteName : {
                required : "姓名不能为空!<br>",
                maxlength : "姓名不能超过4个汉字!<br>"
            },
            whiteSmsNo : {
                required : "电话号码不能为空!<br>",
                digits : "电话号码格式不正确!<br>",
                range : "电话号码格式不正确!<br>"
            }
        },
        submitHandler: function(form) {
            var whiteName = $('#whiteName').val();
            var whiteSmsNo = $('#whiteSmsNo').val();
           //增加白名单
            $.ajax({
                url: 'config/addWhite.htm',
                type:"post",
                data: {
                    targetSn:selectedTargetSn,
                    whiteName:whiteName,
                    whiteSmsNo:whiteSmsNo
                },
                success: function(result) {
                    if(result=="ok") {
                        $("#whiteNo").append("<li class='li-white'><span>"+whiteSmsNo+"("+whiteName+")"+"</span><a class='deleteWhiteLi'><p><img src='resources/images/del.png' width='16' height='16' /></p></a></li>");
                        $("li.li-white").mouseleave(function() {
                            $(this).css("background","white");
                        });

                       $("li.li-white").click(function() {
                           $(this).css("background","#e3d9cf");
                        });

                        $("li.li-white").hover(function(){$(this).css("background","#e3d9cf");},function(){$(this).css("background","white");});

                        $(".deleteWhiteLi").bind("click", deleteWhiteLi);
                        $("#whiteSmsNo").val("");
                        $.pnotify({
                            pnotify_title: '提示',
                            pnotify_text: '添加成功！',
                            pnotify_type: 'error'
                        });
                    }else if(result=="error") {
                        $.pnotify({
                            pnotify_title: '提示',
                            pnotify_text: '添加失败！',
                            pnotify_type: 'error'
                        });
                    } else {
                        if (result == "overLimited") {
                           result = "最多可以添加30个号码!";
                        } else if (result == "existing") {
                           result = "号码已存在于白名单列表!";
                        }
                        $("#errorDiv").html("<font color='red'>"+result+"</font>").show();
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    $.pnotify({
                        pnotify_title: '错误',
                        pnotify_text: '增加白名单时服务器发生错误！',
                        pnotify_type: 'error'
                   });
                }
            });
        }
    });

    var deleteWhiteLi = function() {
        $(this).parent().css("background","#e3d9cf");
        $( "#dialog:ui-dialog" ).dialog( "destroy" );
        var deleteLi = $(this).parent();
        var whiteLiText = $(this).parent().find("span").text();
        var whiteIndex = whiteLiText.indexOf("(");
        var whiteNo = whiteLiText.substring(0,whiteIndex);
        $( "#delete-confirm" ).dialog({
            resizable: false,
            height:150,
            modal: true,
            buttons: {
                "是": function() {
                    //删除白名单
                    $.ajax({
                        url: 'config/deleteWhite.htm',
                        type:"post",
                        data: {
                            telephoneNumber:whiteNo,
                            targetSn:selectedTargetSn
                        },
                        success: function(result) {
                            if(result) {
                                deleteLi.remove();
                            }else {
                                $.pnotify({
                                    pnotify_title: '提示',
                                    pnotify_text: '删除失败！',
                                    pnotify_type: 'error'
                                });
                            }
                            $("#delete-confirm").dialog( "close" );
                            $("#delete-confirm").hide();
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            $.pnotify({
                                pnotify_title: '错误',
                                pnotify_text: '删除白名单时服务器发生错误！',
                                pnotify_type: 'error'
                            });
                            $("#delete-confirm").dialog( "close" );
                            $("#delete-confirm").hide();
                        }
                    });
                },
                "否": function() {
                    $("#delete-confirm").dialog( "close" );
                    $("#delete-confirm").hide();
                }
            }
        });
    };

    $(".deleteWhiteLi").bind("click", deleteWhiteLi);

    $.each([0,1,2,3,4], function(i){
        $("#telephoneNumber"+i).click(function(){
            $("#targetImage").attr("src","resources/images/wennie"+i+".gif");
        });
        $("#telephoneNumber"+i).focus(function(){
            $("#targetImage").attr("src","resources/images/wennie"+i+".gif");
        });
        $("#nickName"+i).click(function(){
            $("#targetImage").attr("src","resources/images/wennie"+i+".gif");
        });
        $("#nickName"+i).focus(function(){
            $("#targetImage").attr("src","resources/images/wennie"+i+".gif");
        });
        rawKeyDial[i] = [$("#telephoneNumber"+i).val(),$("#nickName"+i).val()];
    });

    $.each([1,2,3], function(i){
        var period = $("#period"+i).val();
        rawQuiet[i] = [period,$("#startTime"+i).val(),$("#endTime"+i).val()];
        for(var j=0;j<7;j++) {
            if(period.charAt(j)=='1'){
                $("#quietPeriods"+j+i).click();
            }else{
                $("#quietPeriods"+j+i).attr('checked',false);
            }
        }
    });

    $.each([1,2,3], function(i){
        var period = $("#clockPeriod"+i).val();
        rawClock[i] = [period,$("#clockTime"+i).val(),$("#clockEnable"+i).attr("checked")];

        for(var j=0;j<7;j++) {
            if(period.charAt(j)=='1'){
                $("#clockPeriods"+j+i).click();
            }else{
                $("#clockPeriods"+j+i).attr('checked',false);
            }
        }
    });

    var rules = {};
    for(var i=0; i<2; i++){
        rules['telephoneNumber'+i] = {required : true, range : [100,999999999999]};
        rules['nickName'+i] = {required : true,maxlength : 4};
    }
    for(var i=2; i<5; i++){
        rules['telephoneNumber'+i] = {range : [100,999999999999]};
        rules['nickName'+i] = {maxlength : 4};
    }
    var messages = {};
    for(var i = 0;i<2;i++){
        messages['nickName'+i] = {required : "不能为空!",maxlength:"不能超过4个汉字"};
        messages['telephoneNumber'+i] = {required : "不能为空!", range : "手机号码格式不正确"};
    }
    for(var i = 2;i<5;i++){
        messages['nickName'+i] = {maxlength:"不能超过4个汉字"};
        messages['telephoneNumber'+i] = {range : "手机号码格式不正确"};
    }
    $("#keyForm").validate({
        errorClass: "warning",
        rules :  rules,
        messages : messages,
        submitHandler: function(form) {
            //提交快速拨号
            $.ajax({
                url: 'config/dialKey.htm',
                type:"post",
                data: $("#keyForm").serialize()+"&targetSn="+selectedTargetSn,
                success: function(result) {
                    if(result==1) {
                        $.pnotify({
                            pnotify_title: '提示',
                            pnotify_text: '操作成功,请一分钟后查看操作记录。',
                            pnotify_type: 'info'
                        });
                        $('.config-tabs').hide();
                        $("#target-config-dialog").hide();
                    }else if(result==0) {
                        $.pnotify({
                            pnotify_title: '提示',
                            pnotify_text: '保存失败！',
                            pnotify_type: 'error'
                        });
                    }else if(result==2) {
                        $.pnotify({
                            pnotify_title: '提示',
                            pnotify_text: '按键1新修改的号码为其他账户的主监护人号码，无法修改为该号码！',
                            pnotify_type: 'error'
                        });
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                   $.pnotify({
                       pnotify_title: '错误',
                       pnotify_text: '保存快速拨号时服务器发生错误！',
                       pnotify_type: 'error'
                   });
                }
            });
        }
    });

    for(var i=0; i<3; i++){
        rules['startTime'+i] = {required : true};
        rules['endTime'+i] = {required : true};
    }
    for(var i = 0;i<3;i++){
        messages['startTime'+i] = {required : ""};
        messages['endTime'+i] = {required : ""};
    }
    $("#quietForm").validate({
        errorClass: "warning",
        errorLabelContainer: $("#quietForm div.error"),
        rules :  rules,
        messages : messages,
        submitHandler: function(form) {
            for(var i=0;i<3;i++) {
                var quietperiod="";
                for(var j=0;j<7;j++) {
                    if($("#quietPeriods"+j+i).attr("checked")){
                        quietperiod = quietperiod.concat("1");
                    }else{
                        quietperiod = quietperiod.concat("0");
                    }
                }
                $("#period"+i).val(quietperiod);
//                var clockEnableChecked = $("#clockEnable"+i).attr("checked");
//                if(clockEnableChecked=="checked" && clockperiod=="0000000") {
//                    return false;
//                }
            }
            //免打扰时段
            $.ajax({
                url: 'config/quiet.htm',
                type:"post",
                data: $("#quietForm").serialize()+"&targetSn="+selectedTargetSn,
                success: function(result) {
                    if(result) {
                        $.pnotify({
                            pnotify_title: '提示',
                            pnotify_text: '操作成功,请一分钟后查看操作记录。',
                            pnotify_type: 'info'
                        });
                        $('.config-tabs').hide();
                        $("#target-config-dialog").hide();
                    }else {
                        $.pnotify({
                            pnotify_title: '提示',
                            pnotify_text: '保存失败！',
                            pnotify_type: 'error'
                        });
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                   $.pnotify({
                       pnotify_title: '错误',
                       pnotify_text: '保存免打扰时段时服务器发生错误！',
                       pnotify_type: 'error'
                   });
                }
            });
        }
    });

    $("#smsRemindButton").click( function() {
        //短信提醒
        $.ajax({
            url: 'config/smsRemind.htm',
            type:"post",
            data: $("#smsRemindForm").serialize()+"&targetSn="+selectedTargetSn,
            success: function(result) {
                if(result) {
                    $.pnotify({
                        pnotify_title: '提示',
                        pnotify_text: '操作成功,请一分钟后查看操作记录。',
                        pnotify_type: 'info'
                    });
                    $('.config-tabs').hide();
                    $("#target-config-dialog").hide();
                }else {
                    $.pnotify({
                        pnotify_title: '提示',
                        pnotify_text: '保存失败！',
                        pnotify_type: 'error'
                    });
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                $.pnotify({
                    pnotify_title: '错误',
                    pnotify_text: '保存短信提醒时服务器发生错误！',
                    pnotify_type: 'error'
                });
            }
        });
    });
    for(var i=0; i<3; i++){
        rules['clockTime'+i] = {required : true};
    }
    for(var i = 0;i<3;i++){
        messages['clockTime'+i] = {required : ""};
    }
    $("#clockForm").validate({
        errorClass: "warning",
        errorLabelContainer: $("#clockForm div.error"),
        rules :  rules,
        messages : messages,

        submitHandler: function(form) {
            for(var i=0;i<3;i++) {
                var clockperiod="";
                for(var j=0;j<7;j++) {
                    if($("#clockPeriods"+j+i).attr("checked")){
                        clockperiod = clockperiod.concat("1");
                    }else{
                        clockperiod = clockperiod.concat("0");
                    }
                }
                $("#clockPeriod"+i).val(clockperiod);
                var clockEnableChecked = $("#clockEnable"+i).attr("checked");
                if(clockEnableChecked=="checked" && clockperiod=="0000000") {
                	$.pnotify({
                        pnotify_title: '提示',
                        pnotify_text: '请选择闹钟生效周期!',
                        pnotify_type: 'error'
                    });
                    return false;
                }
            }
            //闹钟时间
            $.ajax({
                cache:false,
                url: 'config/upClock.htm',
                type:"post",
                data: $("#clockForm").serialize()+"&targetSn="+selectedTargetSn,
                success: function(result) {
                    if(result) {
                        $.pnotify({
                            pnotify_title: '提示',
                            pnotify_text: '操作成功,请一分钟后查看操作记录。',
                            pnotify_type: 'info'
                        });
                        $('.config-tabs').hide();
                        $("#target-config-dialog").hide();
                    }else {
                        $.pnotify({
                            pnotify_title: '提示',
                            pnotify_text: '保存失败！',
                            pnotify_type: 'error'
                        });
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                   $.pnotify({
                       pnotify_title: '错误',
                       pnotify_text: '保存闹钟设置时服务器发生错误！',
                       pnotify_type: 'error'
                   });
                }
            });

        }
    });

    $.validator.addMethod("existReceiveNo", function(value, element) {
        var flag=true;
        $.each($("#receiveSelect option").map(function(ul) {
            return [$(this).text()];
        }), function(i, smsNo){
            if(smsNo == value)
                flag= false;
            });
            return flag;
    }, "该号码已经存在于列表！");

    $("#receiveNo").blur(function() {
        $.each($("#smsReceiveListNo li"),function(i){
            var SmsReceiveLiText = $(this).text();
            var SmsReceiveIndex = SmsReceiveLiText.indexOf("(");
            var SmsReceiveNo = SmsReceiveLiText.substring(0,SmsReceiveIndex);
            if($("#receiveNo").val() == SmsReceiveNo) {
                $(this).css("background","#e3d9cf");
                $("#smsReceiveErrorDiv").html("<font color='red'>该号码已存在</font>").show();
            }
        });
    });

    $("#smsReceiveForm").validate({
        errorClass: "warning",
        errorLabelContainer: $("#smsReceiveForm .error"),
        rules : {
            receiveName : {
                required : true,
                maxlength : 4
            },
            receiveNo : {
                required : true,
                phoneNoValidate:true,
                existReceiveNo:true
            }
        },
        messages : {
            receiveName : {
                required : "姓名不能为空!<br>",
                maxlength : "姓名不能超过4个汉字<br>"
            },
            receiveNo : {
                required : "号码不能为空！<br>"
            }
        },
        submitHandler: function(form) {
            var whiteReceiveName = $('#receiveName').val();
            var whiteReceiveNo = $('#receiveNo').val();
            //增加接收短信
            $.ajax({
                url: 'config/addSmsReceive.htm',
                type:"post",
                data: {
                    targetSn:selectedTargetSn,
                    receiveName:whiteReceiveName,
                    receiveNo:whiteReceiveNo
                },
                success: function(result) {
                    if(result=="ok") {
                        $("#smsReceiveListNo").append("<li class='li-white'><span>"+whiteReceiveNo+"("+whiteReceiveName+")"+"</span><a class='deleteSmsReceiveLi'><p><img src='resources/images/del.png' width='16' height='16' /></p></a></li>");

                        $("li.li-white").mouseleave(function() {
                            $(this).css("background","white");
                        });

                        $("li.li-white").click(function() {
                            $(this).css("background","#e3d9cf");
                        });

                        $("li.li-white").hover(function(){$(this).css("background","#e3d9cf");},function(){$(this).css("background","white");});

                        $(".deleteSmsReceiveLi").bind("click", deleteSmsReceiveLi);
                        $("#receiveNo").val("");
                        $.pnotify({
                            pnotify_title: '提示',
                            pnotify_text: '添加成功！',
                            pnotify_type: 'error'
                        });
                    }else if(result=="error") {
                        $.pnotify({
                            pnotify_title: '提示',
                            pnotify_text: '添加失败！',
                            pnotify_type: 'error'
                        });
                    } else {
                        $("#smsReceiveErrorDiv").html("<font color='red'>"+result+"</font>").show();
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    $.pnotify({
                        pnotify_title: '错误',
                        pnotify_text: '保存短信白名单时服务器发生错误！',
                        pnotify_type: 'error'
                    });
                }
            });
        }
    });

    var deleteSmsReceiveLi = function() {

        $( "#dialog:ui-dialog" ).dialog( "destroy" );
        var deleteSmsReceiveLi = $(this).parent();
       var SmsReceiveLiText = $(this).parent().find("span").text();
        var SmsReceiveIndex = SmsReceiveLiText.indexOf("(");
        var SmsReceiveNo = SmsReceiveLiText.substring(0,SmsReceiveIndex);
        $( "#delete-confirm" ).dialog({
            resizable: false,
            height:150,
            modal: true,
            buttons: {
                "是": function() {
                    //删除接收短信
                    $.ajax({
                        url: 'config/deleteSmsReceive.htm',
                        type:"post",
                        data: {
                            smsReceiveTel:SmsReceiveNo,
                            targetSn:selectedTargetSn
                        },
                        success: function(result) {
                            if(result) {
                                deleteSmsReceiveLi.remove();
                            }else {
                                $.pnotify({
                                    pnotify_title: '提示',
                                    pnotify_text: '删除失败！',
                                    pnotify_type: 'error'
                                });
                            }
                            $("#delete-confirm").dialog( "close" );
                            $("#delete-confirm").hide();
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            $.pnotify({
                                pnotify_title: '错误',
                                pnotify_text: '删除短信白名单时服务器发生错误！',
                                pnotify_type: 'error'
                            });
                            $("#delete-confirm").dialog( "close" );
                            $("#delete-confirm").hide();
                        }
                    });
                },
                "否": function() {
                    $("#delete-confirm").dialog( "close" );
                    $("#delete-confirm").hide();
                }
            }
        });
    };

    $(".deleteSmsReceiveLi").bind("click", deleteSmsReceiveLi);

    var contactsData = "";

    $(".phoneBookDeleteButton").click(function() {
        $("#phoneBookForm .error").html("");
        var trId = $(this).parent().parent().attr("id");

        var contactName = $("#"+trId).find("td")[0].innerHTML;
        var contactTelephoneNumber = $("#"+trId).find("td")[1].innerHTML;
        var index = trId.substring(9,trId.length);
        var contactSid = $("#phoneBookSid"+index).val();
        contactsData+="[\""+contactSid+"\",\""+contactName+"\",\""+contactTelephoneNumber+"\",\"1\"],";

        $("#phoneBook"+index).remove();
        var rawPhoneBookSize = $("#phoneBookSize").val();
        $("#phoneBookSize").val(parseInt(rawPhoneBookSize)-parseInt("1"));
        $("#contactSize").html($("#phoneBookSize").val());
    });

    $("#phoneBookAddButton").click(function() {
    	$("#phoneBookForm .error").html("");
        var rawPhoneBookSize = $("#phoneBookSize").val();

        var contactName = $("#contactName").val();
        var contactTelephoneNumber = $("#contactTelephoneNumber").val();

        if(contactName == "") {
            $("#phoneBookForm .error").html("<font color='red'>请输入姓名!</font>");
            return;
        }

        if(contactName.length > 4 ) {
            $("#phoneBookForm .error").html("<font color='red'>姓名不能超过4个汉字!</font>");
            return;
        }

        if(contactTelephoneNumber == "") {
            $("#phoneBookForm .error").html("<font color='red'>请输入号码!</font>");
            return;
        }

        if (contactTelephoneNumber.length <3 || contactTelephoneNumber.length >12) {
            $("#phoneBookForm .error").html("<font color='red'>号码格式不正确!</font>");
            return;
        }

        if (isNaN(contactTelephoneNumber)) {
            $("#phoneBookForm .error").html("<font color='red'>号码格式不正确!</font>");
            return;
        }

        for(var j=1;j<=rawPhoneBookSize;j++) {
            var phoneBookTel = $("#phoneBookTelephoneNumber"+j).val();
            if(typeof(phoneBookTel) != "undefined") {
                if(phoneBookTel == contactTelephoneNumber) {
                    $("#phoneBookForm .error").html("<font color='red'>该号码已存入通讯簿!</font>");
                    $("#phoneBookForm .error").show();
                    return;
                }
            }
        }

        if (rawPhoneBookSize >= 30) {
            $("#phoneBookForm .error").html("<font color='red'>通讯簿最多只能添加30个联系人!</font>");
            $("#phoneBookForm .error").show();
            return;
        }

        $("#phoneBookSize").val(parseInt(rawPhoneBookSize)+parseInt("1"));
        $("#contactSize").html($("#phoneBookSize").val());
        var newPhoneBookSize = $("#phoneBookSize").val();

        var addHtml = "<tr id='phoneBook"+newPhoneBookSize+"'><td width='67' class='col2 contacts_indent'>"+contactName+"</td><td width='108' class='col2'>"+contactTelephoneNumber+"</td><td width='95' class='col2'>添加</td><td width='95' class='col2'>未同步</td><td width='30' class='col2'><a id='phoneBookDeleteButton"+newPhoneBookSize+"' href='#'><img src='resources/images/dynamic-delete.png'></a><input type='hidden' name='phoneBookSid"+newPhoneBookSize+"' value=''><input name='phoneBookName"+newPhoneBookSize+"' value='"+contactName+"' type='hidden'/><input id='phoneBookTelephoneNumber"+newPhoneBookSize+"' name='phoneBookTelephoneNumber"+newPhoneBookSize+"' value='"+contactTelephoneNumber+"' type='hidden'/></td></tr>";
        $(addHtml).insertAfter("#phoneBook-th");
        $("#contactName").val("");
        $("#contactTelephoneNumber").val("");
        $("#phoneBookForm .error").html("");
        contactsData+="[\"\",\""+contactName+"\",\""+contactTelephoneNumber+"\",\"0\"],";

        $("#phoneBookDeleteButton"+newPhoneBookSize).click(function() {
            $("#phoneBookForm .error").html("");
            var trId = $(this).parent().parent().attr("id");
            var index = trId.substring(9,trId.length);
            $("#phoneBook"+index).remove();
            var rawContactSize = $("#phoneBookSize").val();
            $("#phoneBookSize").val(parseInt(rawContactSize)-parseInt("1"));
            $("#contactSize").html($("#phoneBookSize").val());
        });
    });

    $("#phoneBookClearButton").click(function(){
    	$( "#dialog:ui-dialog" ).dialog( "destroy" );
        $("#phone-book-dialog-confirm").dialog({
            resizable: false,
            height:150,
            modal: true,
            buttons: {
                "是": function() {
                    $.ajax({
                        url: 'config/clearPhoneBook.htm',
                        type:"post",
                        data: "targetSn="+selectedTargetSn,
                        success: function(result) {
                            if(result) {
                                $.pnotify({
                                    pnotify_title: '提示',
                                    pnotify_text: '操作成功,请一分钟后查看操作记录。',
                                    pnotify_type: 'info'
                                });
                                $('.config-tabs').hide();
                                hideTargetConfig();
                            }else {
                                $.pnotify({
                                    pnotify_title: '提示',
                                    pnotify_text: '保存失败！',
                                    pnotify_type: 'error'
                                });
                            }
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            $.pnotify({
                                pnotify_title: '错误',
                                pnotify_text: '清空亲子机通讯簿时服务器发生错误！',
                                pnotify_type: 'error'
                            });
                        }
                    });
                    $(this).dialog( "close" );
                    hideTargetConfig();
                },
                "否": function() {
                	$(this).dialog( "close" );
                    $("#phone-book-dialog-confirm").hide();
                }
            }
        });
    });

    $("#phoneBookForm").validate({
        errorClass: "warning",
        errorLabelContainer: $("#phoneBookForm .error"),
        rules : {
        	contactName : {
                maxlength : 4
            },
            contactTelephoneNumber : {
            	digits : true,
            	minlength : 3,
            	maxlength : 12
            }
        },
        messages : {
        	contactName : {
                maxlength : "姓名不能超过4个汉字!<br>"
            },
            contactTelephoneNumber : {
                digits : "电话号码格式不正确!<br>",
                minlength : "电话号码格式不正确!<br>",
                maxlength : "电话号码格式不正确!<br>"
            }
        },
        submitHandler: function(form) {
            $("#phoneBookForm .error").html("");
            $.ajax({
            	beforeSend: function(xhr) {
            		var rawPhoneBookSize = $("#phoneBookSize").val();
                    var contactTelephoneNumber = $("#contactTelephoneNumber").val();
                    for(var j=1;j<=rawPhoneBookSize;j++) {
                        var phoneBookTel = $("#phoneBookTelephoneNumber"+j).val();
                        if(typeof(phoneBookTel) != "undefined") {
                            if(phoneBookTel == contactTelephoneNumber) {
                                $("#phoneBookForm .error").html("<font color='red'>该号码已存入通讯簿!</font>");
                                $("#phoneBookForm .error").show();
                                return false;
                            }
                        }
                    }
            	},
                url: 'config/updatePhoneBook.htm',
                type:"post",
                data: $("#phoneBookForm").serialize()+"&targetSn="+selectedTargetSn+"&contactsData=["+contactsData.substring(0,contactsData.length-1)+"]",
                success: function(result) {
                    if(result=='ok') {
                        $.pnotify({
                            pnotify_title: '提示',
                            pnotify_text: '操作成功,请一分钟后查看操作记录。',
                            pnotify_type: 'info'
                        });
                        $('.config-tabs').hide();
                        $("#target-config-dialog").hide();
                    }else {
                        $("#phoneBookForm .error").html(result);
                        return;
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    $.pnotify({
                        pnotify_title: '错误',
                        pnotify_text: '添加亲子机联系人时服务器发生错误！',
                        pnotify_type: 'error'
                    });
                }
            });
        }
    });
});