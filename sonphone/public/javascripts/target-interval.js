$(function(){
	var rawInterval = [];
	
    $(".his_input1").timePicker();
    $("input:button,input:submit").button();
    $('.input-check').checkbox();
    $("#interval-icon-close-circle").click(function(){
    	var modifyFlag = false;
    	var currentInterval = [];
    	for(var i=0; i<4; i++){
    		currentInterval[i] = [$("#startTime"+i).val(),$("#endTime"+i).val(),$("#periods"+i).find("option:selected").val(),$("#enable"+i).attr("checked")];
    		for(var j=0;j<4;j++) {
    			if(rawInterval[i][j]!=currentInterval[i][j]) {
    				modifyFlag = true;
    				break;
    			}
    		}
    	}
    	
    	if(modifyFlag) {
    		$( "#dialog:ui-dialog" ).dialog( "destroy" );
    		$( "#dialog-confirm" ).dialog({
    			resizable: false,
    			modal: true,
    			buttons: {
    				"是": function() {
    					$('#intervalForm').ajaxSubmit(options);
    					$( this ).dialog( "close" );
    					$("#interval-dialog").hide();
    				},
    				"否": function() {
    					$( this ).dialog( "close" );
    					$("#interval-dialog").hide();
    				}
    			}
    		});	
    	} else {
			// $(this).dialog( "close" );
			$("#interval-dialog").hide();
    	}
    	$("#mDSDW").toggleClass("selected",false);
    });
    
    var rules = {};
    for(var i=0; i<4; i++){
    	rawInterval[i] = [$("#startTime"+i).val(),$("#endTime"+i).val(),$("#periods"+i).find("option:selected").val(),$("#enable"+i).attr("checked")];
    	
        rules['startTime'+i] = {required : true};
        rules['endTime'+i] = {required : true};
    }

    var messages = {};
    for(var i = 0;i<4;i++){
        messages['startTime'+i] = {required : ""};
        messages['endTime'+i] = {required : ""};
    }
    $("#intervalForm").validate({
        errorClass: "warning",
        errorLabelContainer: $("#intervalForm div.error"),
        rules :  rules,
        messages : messages
    });
  
    var options = {
    		beforeSubmit: function() {
    			for(var i = 0;i<4;i++){
    				if($("#startTime"+i).val()>=$("#endTime"+i).val()) {
    					$("#intervalForm div.error").html("时段"+(i+1)+"的开始时间不能大于或者等于结束时间");
    					$("#intervalForm div.error").show();
    					return false;
    				}
    		    }
    		},
            data: {'targetSn': selectedTargetSn},
            success: function(data) {
            	$("#interval-dialog").hide();
                $.pnotify({
                    pnotify_title: '成功',
                    pnotify_text: data,
                    pnotify_type: 'info'
                });
            }
    };
    $('#intervalForm').ajaxForm(options);
});