$(function(){

    $("#fence-list-icon-close-circle").click(function(){
        $("#fence-list-dialog").hide();
        var curTargetFinalStatus = $.findBySelectedTargetSn();
        createTargetMarker(curTargetFinalStatus);
        $("#fenceList").toggleClass("selected",false);
    });

    var findFenceBySid =function(sid) {
        for (var i = 0; i < fenceArray.length; i++) {
            if(sid == fenceArray[i].sid) {
                return fenceArray[i];
            }
        }
    };
    var loadFence = function(sid){
         $("#fence-update-div").load("fence/showUpdate.htm"
                    ,{'sid':sid, 'targetSn': selectedTargetSn}
                    ,function(responseText, textStatus, XMLHttpRequest){
//                         $("#dialogDiv").dialog("open");
                        $("#fence-list-dialog").hide();
                        $("#fence-update-div").show();
                        editFenceMarker(findFenceBySid(sid));
                    }
                );
    };
    var editFenceHandler = function (event){
        var index = event.data.index;
        loadFence(fenceArray[index].sid);
    };

    $.each(fenceArray, function(i, fence){
        $("#fenceOperation"+i).bind("click", {'index': i}, editFenceHandler);
    });
});