// console.log 'app.js loaded!'
var mapInit = function(currentTargetSn){
    var winHeight = getWindowHeight();
    var winWidth = getWindowWidth();
    maplet = new BMap.Map("map");            // 创建Map实例
    var point = new BMap.Point(121.582382,31.209391);    // 创建点坐标
    maplet.centerAndZoom(point,15);                     // 初始化地图,设置中心点坐标和地图级别。
    maplet.enableScrollWheelZoom();                            //启用滚轮放大缩小
    maplet.addControl(new BMap.NavigationControl());
}
$(function(){
    mapInit("");
    $("#accordion").accordion();
})
