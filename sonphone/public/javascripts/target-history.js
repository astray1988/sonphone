$(function(){
     $.extend({
         hideHistoryDialog:function(){
            if(!($("#history-icon-close-circle").is(":hidden"))){
                $("#history-dialog").hide();
                //$("#history-div").hide();
                removeAllPolylines();
                if(typeof(playTimer) != "undefined") {
                	clearInterval(playTimer);
            	}
                var curTargetFinalStatus = $.findBySelectedTargetSn();
                createTargetMarker(curTargetFinalStatus);
                
                $.each(fenceArray, function(i, fence){
                    createFenceMarker(fence);
                });
            }
        }
      });
    var targetStatusArray = null;
    $(".datepicker").datepicker();
    $(".select_inp").timePicker();
    $("input:button,input:submit").button();
    
    $("#history-icon-close-circle").click(function(){
       $.hideHistoryDialog();
       $("#mLSGJ").toggleClass("selected",false);
    });
    
    $("#startDay").change(function(){
    	var startDay = new Date($("#startDay").val());
    	var endDate = new Date();
    	endDate.setTime(startDay.getTime()+24*60*60*1000);
    	var endDateMonth = endDate.getMonth()+1;
    	var endDateDay = endDate.getDate();
    	var endDateStr = endDate.getFullYear();
    	if(parseInt(endDateMonth)<10) {
    		endDateStr += "-0"+endDateMonth;
    	}else {
    		endDateStr += "-"+endDateMonth;
    	}
    	if(parseInt(endDateDay)<10) {
    		endDateStr += "-0"+endDateDay;
    	}else {
    		endDateStr += "-"+endDateDay;
    	}
    	$("#endDay").val(endDateStr);
     });
    
    $("#historyForm").validate({
        errorClass: "warning",
        errorLabelContainer: $("#historyForm div.error"),
        rules : {
            startDay : {
                required : true
            },
            startHistoryTime : {
                required : true
            },
            endDay : {
                required : true
            },
            endHistoryTime : {
                required : true
            }
        },
        messages : {
            startDay : {
                required : ""
            },
            startHistoryTime : {
                required : ""
            },
            endDay : {
                required : ""
            },
            endHistoryTime : {
                required : ""
            }
        },
        submitHandler: function(form) {
        	
        	var startDate = $("#startDay").val();
        	var endDate = $("#endDay").val();
        	
        	var startMonth = startDate.substring(5,startDate.lastIndexOf ('-'));  
        	var startDay = startDate.substring(startDate.length,startDate.lastIndexOf ('-')+1);  
        	var startYear = startDate.substring(0,startDate.indexOf ('-'));  
        	  
        	var endMonth = endDate.substring(5,endDate.lastIndexOf ('-'));  
        	var endDay = endDate.substring(endDate.length,endDate.lastIndexOf ('-')+1);  
        	var endYear = endDate.substring(0,endDate.indexOf ('-'));  
        	
        	var diffResult = ((Date.parse(endMonth+'/'+endDay+'/'+endYear)- Date.parse(startMonth+'/'+startDay+'/'+startYear))/86400000);  
        	if(diffResult < 0) {
        		$("#historyOperationResultDiv").html("<b class=\"warning_img\"><img src=\"resources/images/warning.png\" width=\"16\" height=\"15\" /></b><span style=\"color:red\">开始时间不能大于结束时间!</span>").show();
        		return;
        	}
        	if(diffResult > 2) {
        		$("#historyOperationResultDiv").html("<b class=\"warning_img\"><img src=\"resources/images/warning.png\" width=\"16\" height=\"15\" /></b><span style=\"color:red\">开始时间与结束时间之间的间隔不能超过2天!</span>").show();
        		return;
        	}
        	$("#historyOperationResultDiv").html("");
        	$("#playPointBtn").removeClass("his_butstop").addClass("his_butgo");
        	clearInterval(playTimer);
            $.ajax({
                 url: "target/status/listHistory.htm",
                 type:"post",
                 data: "targetSn="+selectedTargetSn+"&"+$("#historyForm").serialize(),
                 success: function(data) {  
                	 
                     targetStatusArray  = $.parseJSON(data);
                     removeAllMarker();
                     //removeAllPolylines();
                     //removeAllFenceMarker();
                     if(targetStatusArray.length>0) {
                    	 $("#playPointBtn").attr("disabled", false);
                         $.each(targetStatusArray, function(i,targetStatus){
                        	createHistoryMarker(i,targetStatusArray.length,targetStatus);
                         });
                         $("#historyMarkerResultDiv").html("<span style=\"color:blue\">共搜索到"+targetStatusArray.length+"个轨迹点</span>").show();
                     }else {
                         $("#playPointBtn").attr("disabled", true);
                         $("#historyMarkerResultDiv").html("<span style=\"color:blue\">没有定位记录</span>").show();
                     }
                 },
                 error: function (XMLHttpRequest, textStatus, errorThrown) { 
                    $.pnotify({
                        pnotify_title: '错误',
                        pnotify_text: '历史轨迹查询时服务器发生错误！',
                        pnotify_type: 'error'
                    });
                }
            });
        }
    });
    /*$('#queryHistoryBtn').focus();*/
    var playTimer=null;
    
    $("#playPointBtn").click( function() {//播放轨迹点
    	if($(this).attr("class").indexOf("his_butgo")<0) {
    		$(this).removeClass("his_butstop").addClass("his_butgo");
    		clearInterval(playTimer);
    		return;
    	} else {
    		$(this).removeClass("his_butgo").addClass("his_butstop");
    	}
    	
//        removeAllMarker();
        removeAllPolylines();
        //var line_brush = new MBrush();  
        //line_brush.arrow = true; 
        //line_brush.stroke = 3;
        if(targetStatusArray==null) {
        	targetStatusArray = [];
        }
        var arrayIndex=0;
        playTimer = setInterval(function(){
        	 var targetStatusArrayLength = targetStatusArray.length;
        	 var targetStatus = targetStatusArray[arrayIndex];
             if((targetStatusArrayLength==0) || (arrayIndex == targetStatusArrayLength -1)) {
            	 createHistoryMarker(arrayIndex,targetStatusArrayLength,targetStatus);
                 if(targetStatusArrayLength!=0) {
                 	$("#historyOperationResultDiv").html("<span style=\"font-size:14px;color:black\">播放结束</span>").show();
                 	clearInterval(playTimer);
                 	$("#playPointBtn").removeClass("his_butstop").addClass("his_butgo");
                 }
             } else {
                 createHistoryPolyline(arrayIndex,targetStatusArrayLength,targetStatus,targetStatusArray[arrayIndex+1]);
             }

        	arrayIndex++;
        },1000);
        
    });
    
   /* $("#history-list-btn").click(function(){
        if(($("#history-list").is(":hidden"))){
            var historyList = "";
            $.each(targetStatusArray, function(i, targetStatus){
            	historyList +="<li><div class=\"local_t\"><em>定位时间：</em><span>"+new Date(targetStatus.reportTime).format("yyyy-MM-dd hh:mm")+"</span></div>";
                historyList +="<div class=\"local_t\"><em>地址：</em><b>"+targetStatus.address+"</b></div><li>";
            });
            $("#history-list").html(historyList);
            $("#history-list").slideDown();
        }else{
            $("#history-list").slideUp();
        }
    });*/
    
});