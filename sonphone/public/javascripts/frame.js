var allTargetFinalStatusArray = [];

var fenceArray=[];
var selectedTargetSn = null;

var editMEllipse = null;
var editFence = null;

$(function() {

    $("input:button,input:submit,a.button").button();
    
    $(".top_nav ul li a").click(function () {
        $(".top_nav ul li a.selected").toggleClass("selected", false);
        $(this).toggleClass("selected", true);
    });
    
    $("#tooltabs").click(function(){
        measure();
    });
   
    /*$("#menu-target").fancybox({
    'width'             : '66%',
    'height'            : '50%',
    'autoScale'         : false,
    'transitionIn'      : 'none',
    'transitionOut'     : 'none',
    'type'              : 'iframe',
    'hideOnOverlayClick': false,
    'showCloseButton':true
   });*/
/*
    $("#demonstrate").fancybox({
        'width'             : '40%',
        'height'            : '45%',
        'autoScale'         : false,
        'transitionIn'      : 'none',
        'transitionOut'     : 'none',
        'type'              : 'iframe',
        'hideOnOverlayClick': false,
        'showCloseButton':true
    });
    $("#userInfoManage").fancybox({
        'width'             : '56%',
        'height'            : '45%',
        'autoScale'         : false,
        'transitionIn'      : 'none',
        'transitionOut'     : 'none',
        'type'              : 'iframe',
        'hideOnOverlayClick': false,
        'showCloseButton':true
    });
    $("#menu-change-pwd").fancybox({
        'width'             : '30%',
        'height'            : '45%',
        'autoScale'         : false,
        'transitionIn'      : 'none',
        'transitionOut'     : 'none',
        'type'              : 'iframe',
        'hideOnOverlayClick': false,
        'showCloseButton':true
    });
    $("#menu-message").fancybox({
        'width'             : '65%',
        'autoScale'         : true,
        'transitionIn'      : 'none',
        'transitionOut'     : 'none',
        'type'              : 'iframe',
        'hideOnOverlayClick': false,
        'showCloseButton':true
    });
*/
    $("#history-dialog").draggable();
    $("#interval-dialog").draggable();
    $("#fence-list-dialog").draggable();
});
(function($) {
  $.extend({
        getFencesByTargetSn : function () {
            $.ajax({
                url: 'fence/getFenceOfTarget.htm',
                data: "targetSn="+selectedTargetSn,
                success: function(data) {
                    //alert("getFencesByTargetSn");
                    fenceArray = $.parseJSON(data);
                    removeAllFenceMarker();
                    if (fenceArray.length>0) {
                        $.each(fenceArray, function(i, fence){
                            createFenceMarker(fence);
                        });
                    } else{
                        $.pnotify({
                            pnotify_title: '错误',
                            pnotify_text: '加载围栏列表错误！',
                            pnotify_type: 'error'
                        });
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    $.pnotify({
                        pnotify_title: '错误',
                        pnotify_text: '获取围栏列表时，服务器发生错误！',
                        pnotify_type: 'error'
                    });
                    
                }
            });
        },
        updateBySelectedTargetSn :function(target) {
            allTargetFinalStatusArray = target;
        },
        findBySelectedTargetSn :function() {
        	return allTargetFinalStatusArray;
        },
        updateFenceArray :function(fence) {
        	 $.each(fenceArray, function(i, f){
        		 if(f.sid == fence.sid){
        			 fenceArray[i]=fence;
        		 }
             });
        	 $.each(fenceArray, function(i, fence){
                 createFenceMarker(fence);
             });
        },
        loadTarget : function(currentTargetSn) {
            $.ajax({
                url: "target/loadTarget.htm?currentTargetSn="+currentTargetSn,
                dataType: "json",
                success: function(result){
                    if(result){
                        var s = "";
                        var index =0;
                        var target = $.parseJSON(result);

                    	var targetPower = target.target_power;
                    	var battery = 0;
                    	var targetType = target.target_type;
                    	var targetPicture = target.target_picture;

                    	if(typeof(targetPower) == "undefined") {
                    		targetPower = 0;
                    	}
                    	
                    	if(targetPower % 20 == 0) {
                    		battery = targetPower;
                    	} else {
                    		battery = Math.floor(targetPower/20)*20;
                    	}

                    	if(typeof(targetType) == "undefined") {
                    		targetType = "i5-1";
                    	}
                    	
                    	selectedTargetSn = currentTargetSn;
                    	
                    	if(null == targetPicture) {
                    		s += "<li id='userTarget"+index+"'><a href='javascript:changeTargetPicture($.findBySelectedTargetSn());' title='更改头像'><img id='userTargetPicture' src='resources/images/"+targetType+".png' width='56' height='58' class='avatar'/></a><div><div id='targetNickName"+selectedTargetSn+"'><span>昵称：</span><b>"+target.target_nick_name+"</b></div><div><span>电量：</span><b>"+targetPower+"%</b>";
                    	} else {
                    		s += "<li id='userTarget"+index+"'><a href='javascript:changeTargetPicture($.findBySelectedTargetSn());' title='更改头像'><img id='userTargetPicture' src='target/loadTargetPicture.htm?targetSn="+selectedTargetSn+"' width='56' height='58' class='avatar'/></a><div><div id='targetNickName"+selectedTargetSn+"'><span>昵称：</span><b>"+target.target_nick_name+"</b></div><div><span>电量：</span><b>"+targetPower+"%</b>";
                    	}
                        
                       
                        s += "<p class='battery"+battery+"'></p></div><div id='terminalStatus"+selectedTargetSn+"'><span>状态：</span><b>";
                        if(target.target_status=="2") {
                        	s += "关机</b></div><input type='hidden' id=target-list"+index+" value="+selectedTargetSn+"></li>";
                        } else {
                        	s += "开机</b></div><input type='hidden' id=target-list"+index+" value="+selectedTargetSn+"></li>";
                        }

                        $("#target-select").val(selectedTargetSn);
                        $("#target-select-ul").html(s);
                    	
                        $.loadMarkers();
                        $.finalStatus();
                        
//                        $.listInterval();
                        $.loadFence();
                        
                    } else {
                        $.pnotify({
                            pnotify_title: '错误',
                            pnotify_text: '加载设备列表失败，请重新登录',
                            pnotify_type: 'error'
                        });
                    }
                },
                error: function(){
                    $.pnotify({
                        pnotify_title: '错误',
                        pnotify_text: '加载设备列表发生错误，请联系管理员',
                        pnotify_type: 'error'
                    });
                }
            });
        },
        loadFence : function(){
            $.getFencesByTargetSn();
        },
        finalStatus: function(){
            //create target-marker
            $.ajax({
                url: 'target/status/finalStatus.htm?currentTargetSn='+selectedTargetSn,
                success: function(data) {
                    allTargetFinalStatusArray = $.parseJSON(data);
                    if (data == undefined) {
                        $.pnotify({
                            pnotify_title: '错误',
                            pnotify_text: '加载target状态错误！',
                            pnotify_type: 'error'
                        });
                    } else if (data!=null ) {
                        var targetFinalStatus  = $.findBySelectedTargetSn();
                        if(targetFinalStatus!=null){
                            createTargetMarker(targetFinalStatus);
                        }else {
                            $.pnotify({
                                pnotify_title: '错误',
                                pnotify_text: '没有定位数据！',
                                pnotify_type: 'error'
                            });
                        }
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    $.pnotify({
                        pnotify_title: '错误',
                        pnotify_text: '加载设备最新状态时服务器发生错误！',
                        pnotify_type: 'error'
                    });
                }
            });
            
            var switchTargetHandler = function() {
                $("#target-switch").fancybox({
                    'width'             : '46%',
                    'height'            : '70%',
                    'autoScale'         : true,
                    'transitionIn'      : 'none',
                    'transitionOut'     : 'none',
                    'type'              : 'iframe',
                    'hideOnOverlayClick': false,
                    'showCloseButton': true
                });
                $("#target-switch").click();
            };
            
            $("#switch-target").bind({"click":switchTargetHandler});
        },
        loadMarkers : function () {
            $.ajax({
                url: 'marker/loadMarkers.htm',
                data: "targetSn="+selectedTargetSn,
                success: function(data) {
//                    var personalMarkerArray = $.parseJSON(data);
//                    if (personalMarkerArray.length>0) {
//                    	removeAllMarker();
//                        $.each(personalMarkerArray, function(i, personalMarker){
//                            createPersonalMarker(personalMarker);
//                        });
//                    } else{
//                        $.pnotify({
//                            pnotify_title: '错误',
//                            pnotify_text: '加载个性标注错误！',
//                            pnotify_type: 'error'
//                        });
//                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    $.pnotify({
                        pnotify_title: '错误',
                        pnotify_text: '获取个性标注时，服务器发生错误！',
                        pnotify_type: 'error'
                    });
                    
                }
            });
        }
        
    });
})(jQuery);
