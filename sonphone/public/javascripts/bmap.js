var maplet = null;
var editMEllipse = null;
var editFence = null;
var mEllipseCenterMarker =null;
var infoWindow = null;

//avBubble.width = 220;
//avBubble.height = 120;

jQuery(
    function() {
    	window.onresize = resizeApp;
       
    }
);



var resizeApp = function() {
    try{
        var winHeight = getWindowHeight();
        if (winHeight > 79) {
            document.getElementById("mapObj").style.height = winHeight-70+"px";
        }
        //maplet.resize(getWindowWidth(), getWindowHeight()-70);
    }catch(d){}
};

var getWindowHeight = function() {
    if (window.self && self.innerHeight) {
        return self.innerHeight;
    } else if (document.documentElement && document.documentElement.clientHeight) {
        return document.documentElement.clientHeight;   
    } else {
        return 0;
    }
};

var getWindowWidth = function(){
    if (window.self && self.innerWidth) {
        return self.innerWidth;
    } else if (document.documentElement && document.documentElement.clientWidth) {
        return document.documentElement.clientWidth;   
    } else {
        return 0;
    }
};

var mapInit = function(currentTargetSn){
	$.ajax({
        type:"POST",  
        // url: 'user/getCurrentUser.htm?currentTargetSn='+currentTargetSn,
        url: 'test.html?currentTargetSn='+currentTargetSn,
        success: function(result){
            if(result=="false") {
                $("#target-activation").fancybox({
                     'width'             : '46%',
                     'height'            : '85%',
                     'autoScale'         : false,
                     'transitionIn'      : 'none',
                     'transitionOut'     : 'none',
                     'type'              : 'iframe',
                     'scrolling'         : 'no',
                     'hideOnOverlayClick': false,
                     'showCloseButton': false
                 });
                 $("#target-activation").click();
                 return;
             } else if(result=="ok"){
            	var winHeight = getWindowHeight();
                var winWidth = getWindowWidth();
                //alert(winHeight+","+winWidth);
                //maplet = new BMap.Map("mapObj");
                maplet = new BMap.Map("mapObj");            // 创建Map实例
                var point = new BMap.Point(121.582382,31.209391);    // 创建点坐标
                maplet.centerAndZoom(point,15);                     // 初始化地图,设置中心点坐标和地图级别。
                maplet.enableScrollWheelZoom();                            //启用滚轮放大缩小
                //maplet.addControl(new BMap.MapTypeControl()); 
                //maplet.addControl(new BMap.MapTypeControl({anchor: BMAP_ANCHOR_TOP_LEFT,mapTypes: [BMAP_NORMAL_MAP,BMAP_HYBRID_MAP]}));
                maplet.addControl(new BMap.NavigationControl()); 
                //maplet.addControl(new BMap.MapTypeControl({anchor: BMAP_ANCHOR_TOP_LEFT}));    //左上角，默认地图控
                //maplet.setCurrentCity("上海");  
                //maplet.clickToCenter=false;
//                maplet.centerAndZoom(new MPoint(116.38672,39.90805), 8);  
                //options = {  
                //显示位置  
                //    location: {  
                        //位置常量值，比如Maplet.LEFT_BOTTOM，默认值为Maplet.LEFT_TOP  
                //        type: Maplet.LEFT_TOP  
                //    },  
                    //设置组成部分可见性  
                //    view: {  
                        //Boolean类型，是否显示平移控件部分，默认值为true。  
                //        pan : true,  
                        //Boolean类型，是否显示放大按钮部分，默认值为true。  
                //        zoomout : true,  
                        //Boolean类型，是否显示缩放刻度尺部分，默认值为true。  
                //        ruler : true,  
                        //Boolean类型，是否显示缩小按钮部分，默认值为true。  
                //        zoomin : true  
                //    }  
                };
               // var mStandardControl = new MStandardControl(options);
                //maplet.addControl(mStandardControl);
//                maplet.setScale(true);
               // maplet.showOverview({state:false});
                //maplet.resize(winWidth,winHeight-66);
                $.loadTarget(currentTargetSn);
//                $("#markerButton").click(function() {
//                	MEvent.addListener(maplet,"bookmark",function(data) {
//                	    var mPoint =  new MPoint(data.point.lon, data.point.lat);
//                	    var content = "<div style='height:200px;width:280px;overflow-y: auto'>" +
//                		"<table style='height:200px;width:280px'>" +
//                		"<tr>" +
//                		"<td>标记名:</td>" +
//                		"<td><input id='markerName' name='markerName' type='text'></td>"+
//                		"</tr>" +
//                		"<tr>" +
//                		"<td>描述:</td>" +
//                		"<td><textarea id='markerDescription' name='markerDescription' rows='5' cols='20'></textarea></td>"+
//                		"</tr>" +
//                		"<tr>" +
//                		"<td colspan='2' align='center'><input type='radio' id='markerIcon' name='markerIcon' value='house.png' checked='checked'><img src='resources/images/house.png' width='32' height='32'><input type='radio' id='markerIcon' name='markerIcon' value='balloon.gif'><img src='resources/images/balloon.gif' width='32' height='32'><input type='radio' id='markerIcon' name='markerIcon' value='scopperil.gif'><img src='resources/images/scopperil.gif' width='32' height='32'><input type='radio' id='markerIcon' name='markerIcon' value='board.png'><img src='resources/images/board.png' width='32' height='32'></td>" +
//                		"</tr>" +
//                		"<tr>" +
//                		"<td colspan='2' align='center'><input type='button' id='markerSaveButton' value='保存'></td>" +
//                		"</tr>" +
//                		"</table>" +
//                		"</div>";
//                	    var newmarker = new MMarker(
//                	            mPoint,   
//                	            new MIcon("resources/images/house.png",32,32),
//                	            new MInfoWindow("添加标记",content)
//                	        );
//                	    
//                	    maplet.addOverlay(newmarker);
//                	    newmarker.openInfoWindow();
//                	    MEvent.clearListeners(maplet,"bookmark"); 
//                	    maplet.setMode("pan");
//                	    
//                	    $("#markerSaveButton").click(function(){
//                	    	  var markerName = $("#markerName").val();
//                	    	  var markerIcon = $('input:radio[name=markerIcon]:checked').val();
//                	    	  if(markerName == null || markerName == ''){
//                	    		  alert("标注名称不能为空!");
//                	    		  return;
//                	    	  }
//                	    	  $.post("marker/createPersonalMarker.htm",
//                	    	  {
//                	    	    markerName:$("#markerName").val(),
//                	    	    markerDescription:$("#markerDescription").val(),
//                	    	    markerIcon:markerIcon,
//                	    	    longitude:data.point.lon,
//                	    	    latitude:data.point.lat,
//                	    	    targetSn:currentTargetSn
//                	    	  },
//                	    	  function(data,status){
//                	    	    if(data!=null) {
//                	    	    	newmarker.setIcon(new MIcon("resources/images/"+markerIcon,32,32));
//                	    	    	newmarker.setInfoWindow(new MInfoWindow("添加标记",content));
//                	    	        var newContent = "<div style='height:170px;width:400px;overflow-y: auto'>" +
//                	    	        		"<div style='background:#fff;width:100%'>" +
//                	    	        		"<table style='height:170px;width:400px'>" +
//                	    	        		"<tr>" +
//                	    	        		"<td><img src='marker/loadMarkerPicture.htm?markerSid="+data.sid+"' height='150px' width='150px'></td>" +
//                	    	        		"<td style='float:left;margin-top:5px;margin-left:5px;color:blue;'><font style='font-size:14px;font-weight:bold;color:black;'>描述:</font>"+data.markerDescription+"</td>"+
//                	    	        		"</tr>" +
//                	    	        		"</table>" +
//                	    	        		"</div>" +
//                	    	        		"</div>";
//                	    	        newmarker.info=new MInfoWindow(markerName,newContent);
//                	    	        newmarker.openInfoWindow();
//                	    	    }
//                	    	  });
//                	    	});
//
//
//                	});  
//                    maplet.setMode("bookmark"); 
//                });
            //}
        },
        error: function(result){
            //alert(result);
        },
        complete:function(result){
            //alert(result);
        }
    });
    
};

var measure = function(){
    maplet.setMode("measure");
};

/**
 * 创建历史轨迹marker
 * @param targetStatusArray
 */
var createHistoryMarker = function(i,statusLength,targetStatus){
    var longitude = targetStatus.longitude;
    var latitude = targetStatus.latitude;
    if (longitude!=null && latitude!=null) {
        
        var point =  new BMap.Point(longitude, latitude);

    	var icon ;
        if(i == 0) {
        	icon = new BMap.Icon("resources/images/start_marker.png",{width:29,height:38});
        	icon.setImageSize( new BMap.Size(28,38));
        	maplet.panTo(point);
        } else if(i==(statusLength-1)){
        	icon = new BMap.Icon("resources/images/end_marker.png",{width:29,height:38});
        	icon.setImageSize( new BMap.Size(28,38));
        } else {
        	icon = new BMap.Icon("resources/images/history_marker.gif",{width:32,height:32});
        	icon.setImageSize( new BMap.Size(32,32));
            //icon.setInfoWindowAnchor(new BMap.Size(150,50));
        }
        var newmarker = new BMap.Marker(point,{icon:icon});
        maplet.addOverlay(newmarker);
        //var label = new BMap.Label("我是百度标注哦",{offset:new BMap.Size(20,-10)});
        //newmarker.setLabel(label); //添加百度label
        //alert(point.lng + "," + point.lat);
        var time = new Date(targetStatus.reportTime);
        var opts = {
        		  width : 200,     // 信息窗口宽度
        		  height: 60,     // 信息窗口高度
        		  title : "定位结果("+time.format('yyyy-MM-dd hh:mm:ss')+")" , // 信息窗口标题
        		  enableMessage:false,//设置允许信息窗发送短息
        		  message:"定位结果"
        		}
        var historyInfoWindow  = new BMap.InfoWindow("地址："+targetStatus.address, opts);  // 创建信息窗口对象
//        gc.getLocation(baiduPoint, function(rs){
//            var addComp = rs.addressComponents;
//            var address = addComp.province + addComp.city + addComp.district + addComp.street + addComp.streetNumber;
//            //maplet.openInfoWindow(infoWindow,point); //开启信息窗口
//        }); 
        
        newmarker.addEventListener("click", function(){  
        	this.openInfoWindow(historyInfoWindow,point); //开启信息窗口
        });
    
//        BMap.Convertor.translate(gpsPoint,0,function (baiduPoint){});
    }
};
var createHistoryPlayMarker = function(i,statusLength,targetStatus){
    var longitude = targetStatus.longitude;
    var latitude = targetStatus.latitude;
    if (longitude!=null && latitude!=null) {
        
        var point =  new BMap.Point(longitude, latitude);
        var newmarker = new BMap.Marker(point);
        maplet.addOverlay(newmarker);
        //var label = new BMap.Label("我是百度标注哦",{offset:new BMap.Size(20,-10)});
        //newmarker.setLabel(label); //添加百度label
        //alert(point.lng + "," + point.lat);
        var time = new Date(targetStatus.reportTime);
        var opts = {
        		  width : 200,     // 信息窗口宽度
        		  height: 60,     // 信息窗口高度
        		  title : "定位结果("+time.format('yyyy-MM-dd hh:mm:ss')+")" , // 信息窗口标题
        		  enableMessage:false,//设置允许信息窗发送短息
        		  message:"定位结果"
        		}
        var historyInfoWindow = new BMap.InfoWindow("地址："+targetStatus.address, opts);  // 创建信息窗口对象
//        gc.getLocation(baiduPoint, function(rs){
//            var addComp = rs.addressComponents;
//            var address = addComp.province + addComp.city + addComp.district + addComp.street + addComp.streetNumber;
//            //maplet.openInfoWindow(infoWindow,point); //开启信息窗口
//        }); 
        
        newmarker.addEventListener("click", function(){  
        	this.openInfoWindow(historyInfoWindow,point); //开启信息窗口
        });
    
    }
};
var createHistoryPolyline = function(i,targetStatusArrayLength,targetStatus,nextTargetStatus) {
	
	
	
//    var latitude = targetStatus.latitude;
//    var mPoint =  new MPoint(longitude, latitude);
//    
//    var nextLongitude = nextTargetStatus.longitude;
//    var nextLatitude = nextTargetStatus.latitude;
//    var nextPoint =  new MPoint(nextLongitude, nextLatitude);
//    
//	var polyline = new MPolyline(  
//	        [  
//	         	mPoint,
//	         	nextPoint
//	        ],brushLine
//	    );  
//
	
	createHistoryMarker(i,targetStatusArrayLength,targetStatus);
	var beforePoint = new BMap.Point(targetStatus.longitude, targetStatus.latitude);
	var nextPoint = new BMap.Point(nextTargetStatus.longitude, nextTargetStatus.latitude);
    var polyline = new BMap.Polyline([ 
      beforePoint,
      nextPoint
      ],  
      {strokeColor:"blue", strokeWeight:2, strokeOpacity:0.5}  
    );  
    maplet.panTo(nextPoint);      
    maplet.addOverlay(polyline); 

    
//	BMap.Convertor.translate(beforePoint,0,function (beforeBaiduPoint){
//		BMap.Convertor.translate(nextPoint,0,function (nextBaiduPoint){});
//    });
//	maplet.addOverlay(polyline);
//	if(polyline.getCenterXY().x<0 || polyline.getCenterXY().x>getWindowWidth()||polyline.getCenterXY().y<0||polyline.getCenterXY().y>getWindowHeight()) {
//		maplet.setCenter(nextPoint); 
//	}
//	var myIcon = new BMap.Icon("Mario.png", new BMap.Size(32, 70), {    //小车图片
//	    //offset: new BMap.Size(0, -5),    //相当于CSS精灵
//	    imageOffset: new BMap.Size(0, 0)    //图片的偏移量。为了是图片底部中心对准坐标点。
//	  });
//	var myP1 = new BMap.Point(targetStatus.longitude, targetStatus.latitude);    //起点
//	var myP2 = new BMap.Point(nextTargetStatus.longitude,nextTargetStatus.latitude);    //终点
//
//	var driving2 = new BMap.DrivingRoute(maplet, {renderOptions:{map: maplet, autoViewport: true}});    //驾车实例
//	driving2.search(myP1, myP2);    //显示一条公交线路
//	window.run = function (){
//	    var driving = new BMap.DrivingRoute(maplet);    //驾车实例
//	    driving.search(myP1, myP2);
//	    driving.setSearchCompleteCallback(function(){
//	        var pts = driving.getResults().getPlan(0).getRoute(0).getPath();    //通过驾车实例，获得一系列点的数组
//	        var paths = pts.length;    //获得有几个点
//
//	        var carMk = new BMap.Marker(pts[0],{icon:myIcon});
//	        maplet.addOverlay(carMk);
//	        i=0;
//	        function resetMkPoint(i){
//	            carMk.setPosition(pts[i]);
//	            if(i < paths){
//	                setTimeout(function(){
//	                    i++;
//	                    resetMkPoint(i);
//	                },100);
//	            }
//	        }
//	        setTimeout(function(){
//	            resetMkPoint(5);
//	        },100)
//
//	    });
//	}
//
//	setTimeout(function(){
//	    run();
//	},1000);
};

/**
 * 创建设备 marker
 * @param target
 */
var createTargetMarker =  function(target) {
    createMarker(target);
//    var newmarker = createMarker(target);
//    var longitude = target.longitude;
//    var latitude = target.latitude;
//    if (longitude!=null && latitude!=null) {
//    	var mPoint =  new BMap.Point(longitude, latitude);
//    	maplet.setCenter(mPoint);
//    }
//    return newmarker;
};
/**
 *更新
 */
var updateMarker = function(target) {
    var longitude = target.longitude;
    var latitude = target.latitude;
    if (longitude!=null && latitude!=null 
    		&& parseInt(longitude) != 0 
    		&& parseInt(latitude) != 0) {
        var point =  new BMap.Point(longitude, latitude);
    	var icon = new BMap.Icon("resources/images/onemarker.gif",{width:32,height:32});
    	icon.setImageSize( new BMap.Size(32,32));
        var newmarker = new BMap.Marker(point,{icon:icon});
        maplet.addOverlay(newmarker);
        //var label = new BMap.Label("我是百度标注哦",{offset:new BMap.Size(20,-10)});
        //newmarker.setLabel(label); //添加百度label
        maplet.centerAndZoom(point, 15);
        //alert(point.lng + "," + point.lat);
        var time = new Date(target.reportTime);
        var opts = {
        		  width : 200,     // 信息窗口宽度
        		  height: 60,     // 信息窗口高度
        		  title : "定位结果("+time.format('yyyy-MM-dd hh:mm:ss')+")" , // 信息窗口标题
        		  enableMessage:false,//设置允许信息窗发送短息
        		  message:"定位结果"
        		};

        infoWindow = new BMap.InfoWindow("地址："+target.address, opts);  // 创建信息窗口对象
//        gc.getLocation(point, function(rs){
//            var addComp = rs.addressComponents;
//            var address = addComp.province + addComp.city + addComp.district + addComp.street + addComp.streetNumber;
//            maplet.openInfoWindow(infoWindow,point); //开启信息窗口
//        }); 
        
        newmarker.openInfoWindow(infoWindow,point); //开启信息窗口
        newmarker.addEventListener("click", function(){  
        	this.openInfoWindow(infoWindow,point); //开启信息窗口
        });
    
    }
};

/**
 * 创建marker
 * @param target
 */
var createMarker = function(target) {
    var longitude = target.longitude;
    var latitude = target.latitude;
    if (longitude!=null && latitude!=null 
    		&& parseInt(longitude) != 0 
    		&& parseInt(latitude) != 0) {
        var point =  new BMap.Point(longitude, latitude);

    	var icon = new BMap.Icon("resources/images/onemarker.gif",{width:32,height:32});
    	icon.setImageSize( new BMap.Size(32,32));
        var newmarker = new BMap.Marker(point,{icon:icon});
        maplet.addOverlay(newmarker);
        //var label = new BMap.Label("我是百度标注哦",{offset:new BMap.Size(20,-10)});
        //newmarker.setLabel(label); //添加百度label
        maplet.centerAndZoom(point, 15);
        //alert(point.lng + "," + point.lat);
        var time = new Date(target.reportTime);
        var opts = {
        		  width : 200,     // 信息窗口宽度
        		  height: 60,     // 信息窗口高度
        		  title : "定位结果("+time.format('yyyy-MM-dd hh:mm:ss')+")" , // 信息窗口标题
        		  enableMessage:false,//设置允许信息窗发送短息
        		  message:"定位结果"
        		};

        infoWindow = new BMap.InfoWindow("地址："+target.address, opts);  // 创建信息窗口对象
//        gc.getLocation(point, function(rs){
//            var addComp = rs.addressComponents;
//            var address = addComp.province + addComp.city + addComp.district + addComp.street + addComp.streetNumber;
//            maplet.openInfoWindow(infoWindow,point); //开启信息窗口
//        }); 
        
        newmarker.addEventListener("click", function(){  
        	this.openInfoWindow(infoWindow,point); //开启信息窗口
        });

    
    }
};

/**
 * 移除所有设备marker
 */
var removeAllMarker = function(){
    
//    var markerArray = [];
//    markerArray = maplet.getOverlays();
//    $.each(markerArray, function(i, marker){
//    	if(marker.label==null) {
//    		//maplet.removeOverlay(marker);
//    	}
//    });
	maplet.clearOverlays();
};

/**
 * 移除所有Polylines
 */
var removeAllPolylines = function(){
	maplet.clearOverlays();
};

/**
 *更新设备marker
 * @param target
 */
var updateTargetMarker = function(target) {
	removeAllMarker();
	updateMarker(target);
    $.each(fenceArray, function(i, fence){
        createFenceMarker(fence);
    });
};

/**
 * 创建围栏
 */
var createFenceMarker = function(fence) {
	
	var optsCircle={strokeColor:'#000080',fillColor:'#778899',
			strokeWeight:1,strokeOpacity:1,fillOpacity:0.5,
			strokeStyle:'solid',enableMassClear:true,enableClicking:true};
	
    if(fence.enable){
    	var opts = {
    			  width : 200,     // 信息窗口宽度
    			  height: 60,     // 信息窗口高度
    			  title : "围栏信息" , // 信息窗口标题
    			  enableMessage:false,//设置允许信息窗发送短息
    			};
    	var content="围栏名称："+fence.fenceName+"\</br>围栏半径："+fence.radius+"(米)";//信息窗口内容
    	var infoWindow = new BMap.InfoWindow(content,opts);
    	var longitude = fence.longitude;
    	var latitude = fence.latitude;
    	var mPoint = new BMap.Point(longitude,latitude);
    	var radius = fence.radius;
    	var bFence = new BMap.Circle(mPoint,radius,optsCircle);
    	bFence.addEventListener('click',function(){
    		maplet.openInfoWindow(infoWindow,mPoint);
    	});
    	maplet.addOverlay(bFence);
    }
};
/**
 * 创建编辑围栏
 */
var createEditFenceMarker = function(fence) {
	var strokeColor = "#000080";
	var fillColor = "#778899";
    if(!fence.enable){
    	strokeColor = "#FF0000";
        fillColor = "#FFA07A";
    }
	var optsCircle={strokeColor:strokeColor,fillColor:fillColor,
			strokeWeight:1,strokeOpacity:1,fillOpacity:0.5,
			strokeStyle:'solid',enableMassClear:true,enableClicking:true};
	var longitude = fence.longitude;
	var latitude = fence.latitude;
	var mPoint = new BMap.Point(longitude,latitude);
	var radius = fence.radius;
	var bFence = new BMap.Circle(mPoint,radius,optsCircle);
	maplet.addOverlay(bFence);
    
    return bFence;
};
/**
 * 移除所有围栏marker
 */
var removeAllFenceMarker = function(){
	removeAllMarker();
};


var moveToRecentlyLocation = function(contextMenuItem,menu) {
	var targetFinalStatus = $.findBySelectedTargetSn();
    editFence.longitude = (parseFloat(targetFinalStatus.longitude)+parseFloat(0.001));
    editFence.latitude = (parseFloat(targetFinalStatus.latitude)+parseFloat(0.001));
    
    editFenceMarker(editFence);
};
/**
 * 编辑围栏
 * @param fence_index
 */
var editFenceMarker = function(fence) {
	removeAllFenceMarker();
	editFence =fence;
    
    var mPoint = new BMap.Point(editFence.longitude,editFence.latitude);
    
    var icon = new BMap.Icon("resources/images/marker.gif",{width:32,height:32});
    mEllipseCenterMarker = new BMap.Marker(mPoint,{icon:icon});
    mEllipseCenterMarker.addEventListener('dragging',movemEditEllipse);
    mEllipseCenterMarker.enableDragging();
    maplet.addOverlay(mEllipseCenterMarker);
    
    editMEllipse = createEditFenceMarker(editFence);
    
    maplet.centerAndZoom(mPoint, 15);
};
/**
 * 移除编辑围栏
 */
var removeEditFence = function (enable){
    
    //移除围栏
    maplet.removeOverlay(editMEllipse);
    //移除围栏中心marker
    maplet.removeOverlay(mEllipseCenterMarker);
    
    editFence.enable = enable;
    editFence.radius =$("#fenceRadius").val();
    editFence.fenceName =$("#fenceName").val();
    $.updateFenceArray(editFence);
};
/**
 * 移动编辑围栏
 */
var movemEditEllipse =  function (){
    
    editFence.longitude =mEllipseCenterMarker.getPosition().lng;
    editFence.latitude = mEllipseCenterMarker.getPosition().lat;
    maplet.removeOverlay(editMEllipse);
    editMEllipse = createEditFenceMarker(editFence);
};
/**
 * resize编辑围栏
 */
var resizeEllipse =  function (){
    
    maplet.removeOverlay(editMEllipse);
    editFence.radius = $("#fenceRadius").val();
    editMEllipse = createEditFenceMarker(editFence);
};

/**
 * 创建设备 marker
 * @param target
 */
var changeTargetPicture =  function(target) {
	 $("#target-picture").fancybox({
         'width'             : '35%',
         'height'            : '95%',
         'autoScale'         : false,
         'transitionIn'      : 'none',
         'transitionOut'     : 'none',
         'type'              : 'iframe',
         'scrolling'         : 'no',
         'hideOnOverlayClick': false,
         'showCloseButton': true,
         'onClosed' :function() {
        	 $("#userTargetPicture").attr("src","target/loadTargetPicture.htm?_="+Math.random()+"&targetSn="+target.targetSn);
         }
     });
     $("#target-picture").click();
     return;
};

/**
 * 创建围栏
 */
var createPersonalMarker = function(personalMarker) {
	var longitude = personalMarker.longitude;
	var latitude = personalMarker.latitude;
	var mPoint = new BMap.Point(longitude,latitude);
	var newPersonalMarker;
	if(personalMarker.sid=='4449d1a2-d71c-4666-a0c6-7e14ed777361') {
//		newPersonalMarker = new MMarker(
//                mPoint,   
//                new MIcon("resources/images/LVG.png",32,32),null,new MLabel("LVG",options = {visible: false})
//            );
		//var lvgIcon = new BMap.Icon("resources/images/LVG.png", new BMap.Size(80,60));
		newPersonalMarker = new BMap.Marker(mPoint,{enableMassClear:false});
	} else if (personalMarker.sid=='99038dde-9a87-43b5-abca-c99026c04010') {
//		newPersonalMarker = new MMarker(
//                mPoint,   
//                new MIcon("resources/images/diskon.png",40,25),null,new MLabel("Diskon",options = {visible: false})
//            );
		newPersonalMarker = new BMap.Marker(mPoint,{enableMassClear:false});
	} else {
//		newPersonalMarker = new MMarker(
//                mPoint,   
//                new MIcon("resources/images/"+personalMarker.markerIcon,32,32),null,new MLabel(personalMarker.markerName,options = {visible: false})
//            );
		newPersonalMarker = new BMap.Marker(mPoint,{enableMassClear:false});
	}
    maplet.addOverlay(newPersonalMarker);
    maplet.panTo(mPoint);
//    MEvent.addListener(newPersonalMarker,'click', function(marker) {  
//        var title= personalMarker.markerName;
//        var content = "<div style='height:170px;width:400px;overflow-y: auto'>" +
//        		"<div style='background:#fff;width:100%'>" +
//        		"<table style='height:170px;width:400px'>" +
//        		"<tr>" +
//        		"<td><img src='marker/loadMarkerPicture.htm?markerSid="+personalMarker.sid+"' height='150px' width='150px'></td>" +
//        		"<td style='float:left;margin-top:5px;margin-left:5px;color:blue;'><font style='font-size:14px;font-weight:bold;color:black;'>描述:</font>"+personalMarker.markerDescription+"</td>"+
//        		"</tr>" +
//        		"</table>" +
//        		"</div>" +
//        		"</div>";
//        var mInfoWindow = new MInfoWindow(title,content);
//        marker.info=mInfoWindow;
//        marker.openInfoWindow();
//    });
};