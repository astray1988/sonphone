package controllers;

import play.mvc.With;

/**
 * Created by samliu on 14/12/18.
 */
@With(Secure.class)
public class Configs extends SuperController {

    @Check({"admin"})
    public static void show() {
        render();
    }

}
