package controllers;


import models.annotation.NotCheck;
import org.apache.commons.lang.StringUtils;
import play.Logger;
import play.libs.WS;
import utils.InterfaceDef;


public class Security extends Secure.Security {

    static void onAuthenticated() {

    }

    static boolean authenticate(String username, String password) {
        // WS.HttpResponse hr = WS.url(InterfaceDef.HOST + InterfaceDef.login).post();
        Logger.debug("username: %s", username);
        session.put("username", "tester");
        session.put("userId", "1");
        session.put("role", "admin");
        return true;
    }

    static boolean check(String profile) {
        Logger.debug("profile: %s", profile);
        Logger.debug("role: %s", session.get("role"));
        if (StringUtils.isEmpty(profile) || request.invokedMethod.isAnnotationPresent(NotCheck.class)) {
            return true;
        } else {
            boolean b = profile.equals(session.get("role"));
            Logger.debug("b: %s", b);
            return b;
        }
    }

}
