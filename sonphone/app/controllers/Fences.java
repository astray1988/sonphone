package controllers;

import play.Logger;
import play.mvc.With;

/**
 * Created by samliu on 14/12/18.
 */
@With(Secure.class)
public class Fences extends SuperController {

    @Check({"admin"})
    public static void list() {
        render();
    }

}
