package controllers;

import play.Logger;
import play.mvc.With;

/**
 * Created by samliu on 14/12/18.
 */
@With(Secure.class)
public class Locations extends SuperController {

    @Check({"admin"})
    public static void index() {
        String user = Security.connected();
        Logger.debug("user: %s", user);
        render(user);
    }

}
