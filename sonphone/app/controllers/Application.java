package controllers;

import com.google.gson.JsonElement;
import play.Logger;
import play.Play;
import play.libs.Crypto;
import play.libs.Time;
import play.libs.WS;
import utils.InterfaceDef;

import java.util.Date;

public class Application extends SuperController {

    public static void index() {
        render();
    }

    public static void login() {
        render();
    }

    public static void register() {
        render();
    }

    public static void logout() {
        session.clear();
        Logger.debug("========= logout ===============");
        index();
    }

    /**
     * m.cid = arr[1];
     * m.mac = arr[2];
     * m.imei = arr[3];
     * m.imsi = arr[4];
     * m.os = Integer.parseInt(arr[5]);
     * m.m_number = arr[6].trim();
     * m.pwd = arr[8];
     *
     * @param loginName
     * @param password
     */
    public static void doRegister(String loginName, String password, String authCode) {
        WS.HttpResponse hr = WS.url(InterfaceDef.HOST + InterfaceDef.register)
                .setParameter("z", "MjN8MTIzNDV8MTIsMTQsODcsamosZmZ8MTIzNDU2fDEyMzQ1NnwxfDE1MDAwOTkzNDczfDIwOTQzNnwxMjM0NTZ8MzIx")
                .post();
        JsonElement je = hr.getJson();
        Logger.debug(je.toString());
        login();
    }

    /**
     * @param loginName
     * @param password
     * @param remember
     * @throws Throwable
     */
    public static void doLogin(String loginName, String password, boolean remember) throws Throwable {
        Logger.debug("loginName: %s", loginName);
        boolean allowed = Security.authenticate(loginName, password);
        if (validation.hasErrors() || !allowed) {
            flash.keep("url");
            flash.error("secure.error");
            params.flash();
            login();
        }
        // Mark user as connected
        session.put("username", loginName);
        // Remember if needed
        if (remember) {
            Date expiration = new Date();
            String duration = Play.configuration.getProperty("secure.rememberme.duration", "30d");
            expiration.setTime(expiration.getTime() + Time.parseDuration(duration) * 1000);
            response.setCookie("rememberme", Crypto.sign(loginName + "-" + expiration.getTime()) + "-" + loginName + "-" + expiration.getTime(), duration);

        }
        // Redirect to the original URL (or /)
        redirect("/location");
        // Secure.redirectToOriginalURL();
    }
}