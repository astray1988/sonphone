package utils;

/**
 * Created by samliu on 14/12/19.
 */
public class InterfaceDef {
    public static final String HOST = "http://localhost:9000";
    public static final String checkDigit = "/c/verificationCode";
    public static final String register = "/c/reg";
    public static final String login = "/c/login";
    public static final String logout = "/c/logout";
    public static final String updateMemberInfo = "/c/updateCustomerInfo";
    public static final String getMemberInfo = "/c/getCustomerInfo";
    public static final String download = "/c/download";
    public static final String update = "/c/update";
    public static final String sendResetPasswordMail = "/c/resetpassword";
    public static final String setRWatch = "/c/setUserWatch";
    public static final String changePassword = "/c/changePassword";
    public static final String getRWatchList = "/c/watchList";
    public static final String setElectronicFence = "/c/electronicFence";
    public static final String addRWatch = "/c/bindingWatch";
    public static final String watchInfo = "/c/watchInfo";
    public static final String delRWatch = "/c/unbindingWatch";
    public static final String webBindingWatch = "/c/bindingWatchforWeb";
    public static final String receiver = "/c/sendGPS";
    public static final String getLocation = "/c/searchLocation";
    public static final String getRWatchInfo = "/c/getRWatchInfo";
    public static final String syncTime = "/c/watchSyncTime";

}
